<div align="center">
    <img src="https://storage.googleapis.com/devfloyd/logo.devfloyd.light.png">
</div>

# FEEDBACK RASTREABILITY REPORT 📊

Aplicação para exibição de relatórios de rastreabilidade.

## Ambiente de Desenvolvimento 🧑‍💻

### Requisitos

- Node.js v12.18.3

### Comandos

`yarn`
`yarn start`

<a href="http://floyd.dev.br">dev.floyd</a>
