import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;

  flex-direction: row;
`;

export const Aside = styled.aside`
  width: 30%;
  height: 100%;

  padding: 15px;
`;

export const InfoPanel = styled.div`
  width: 100%;
  height: auto;

  border-radius: 5px;

  background-color: #fff;
  padding: 40px 15px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const QRContainer = styled.div`
  width: '100%';
  height: 256;
`;

export const Info = styled.div`
  margin-top: 25px;

  text-align: center;
`;

export const MapContainer = styled.div`
  width: 70%;
  height: 100%;

  background-color: #fff;
`;

export const MapCanvas = styled.div`
  width: 100%;
  min-height: 600px;
`;

export const RastreabilityContainer = styled.div`
  width: 100%;
  height: auto;

  padding: 20px;

  background-color: #b59377;
`;

export const Rastreability = styled.div`
  width: 100%;
  min-height: 600px;

  border-radius: 5px;

  background-color: #fff;
`;
