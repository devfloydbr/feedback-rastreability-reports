import React, { useCallback, useEffect, useState, useRef } from 'react';
import {
  GoogleMap,
  Marker,
  InfoWindow,
  useLoadScript,
  Polyline,
} from '@react-google-maps/api';
import QRCode from 'qrcode.react';

import api from '../../utils/api';

import Header from '../../components/Header';

import {
  Container,
  Aside,
  MapContainer,
  InfoPanel,
  MapCanvas,
  QRContainer,
  Info,
  RastreabilityContainer,
  Rastreability,
} from './styles';

interface Location {
  lat: number;
  lng: number;
}

interface Rastreabilidade {
  id: number;
  local: {
    nome: string;
    localizacao: Location;
  };
  data: string;
  dias: number;
  qualidade: string;
  unidade: string;
  sacas: number;
  peso: number;
}

interface Documento {
  numero: string;
  data: string;
  safra: string;
  produtor: string;
  avatar: string;
  fazenda: string;
  bebida: {
    tipo: string;
    padrao: string;
    unidade: string;
  };
  locations: Location[];
  rastreabilidades: Rastreabilidade[];
}

interface Resize {
  width: number | undefined;
}

const Dashboard: React.FC = () => {
  const [documento, setDocument] = useState<Documento>();
  const [coordinates, setCoordinates] = useState<Location[]>([]);

  const [dimensions, setDimensions] = React.useState<Resize>({
    width: window.innerWidth,
  });

  const mapCanvasRef = useRef<HTMLDivElement>(null);
  const mapRef = useRef(null);

  // const [center, setCenter] = useState<Location>();
  const [
    selectedMarker,
    setSelectedMarker,
  ] = useState<Rastreabilidade | null>();

  useEffect(() => {
    api.get('documento').then(response => {
      setDocument(response.data);
      setCoordinates(response.data.locations);
    });

    const handleResize = () => {
      setDimensions({
        width: mapCanvasRef.current?.offsetWidth,
      });
    };

    window.addEventListener('resize', handleResize);
  }, []);

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyD68Cs78a83IY7JjuQM8al3BOa605Ks1sM',
  });

  const mapContainerStyle = {
    width: dimensions.width,
    height: 600,
  };

  const options: google.maps.MapOptions = {
    styles: [
      {
        featureType: 'landscape',
        stylers: [
          { hue: '#FFBB00' },
          { saturation: 43.400000000000006 },
          { lightness: 37.599999999999994 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.highway',
        stylers: [
          { hue: '#FFC200' },
          { saturation: -61.8 },
          { lightness: 45.599999999999994 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.arterial',
        stylers: [
          { hue: '#FF0300' },
          { saturation: -100 },
          { lightness: 51.19999999999999 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.local',
        stylers: [
          { hue: '#FF0300' },
          { saturation: -100 },
          { lightness: 52 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'water',
        stylers: [
          { hue: '#0078FF' },
          { saturation: -13.200000000000003 },
          { lightness: 2.4000000000000057 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'poi',
        stylers: [
          { hue: '#00FF6A' },
          { saturation: -1.0989010989011234 },
          { lightness: 11.200000000000017 },
          { gamma: 1 },
        ],
      },
    ],
    disableDefaultUI: true,
    zoomControl: true,
    gestureHandling: 'greedy',
  };

  const mapOnLoad = useCallback(map => {
    mapRef.current = map;
  }, []);

  if (loadError) {
    console.log('Erro ao carregar mapa');
    return null;
  }

  return (
    <>
      <Header />
      <Container>
        <Aside>
          <InfoPanel>
            <QRContainer>
              <QRCode value="http://facebook.github.io/react/" size={256} />
            </QRContainer>
            <Info>
              <h3>{documento?.numero}</h3>
              <h4>{documento?.produtor}</h4>
              <h4>{documento?.data}</h4>
            </Info>
          </InfoPanel>
        </Aside>
        <MapContainer>
          <MapCanvas ref={mapCanvasRef}>
            {isLoaded ? (
              <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={14}
                center={{ lat: -18.669092, lng: -48.177053 }}
                options={options}
                onLoad={mapOnLoad}
              >
                <Polyline
                  path={coordinates}
                  options={{
                    geodesic: true,
                    strokeColor: '#00FF6A',
                    strokeWeight: 8,
                  }}
                />

                {documento
                  ? documento.rastreabilidades.map(marker => (
                      <Marker
                        key={marker.id}
                        position={{
                          lat: marker.local.localizacao.lat,
                          lng: marker.local.localizacao.lng,
                        }}
                        onClick={() => {
                          setSelectedMarker(marker);
                        }}
                      />
                    ))
                  : null}

                {selectedMarker ? (
                  <InfoWindow
                    position={{
                      lat: selectedMarker.local.localizacao.lat,
                      lng: selectedMarker.local.localizacao.lng,
                    }}
                    onCloseClick={() => {
                      setSelectedMarker(null);
                    }}
                    options={{ pixelOffset: new google.maps.Size(0, -42) }}
                  >
                    <div>
                      <h2>Teste</h2>
                      <p>fwefwefwefewf</p>
                    </div>
                  </InfoWindow>
                ) : null}
              </GoogleMap>
            ) : null}
          </MapCanvas>

          <RastreabilityContainer>
            <Rastreability />
          </RastreabilityContainer>
        </MapContainer>
      </Container>
    </>
  );
};

export default Dashboard;
