import {
  GoogleMap,
  InfoWindow,
  Marker,
  Polyline,
  useLoadScript,
} from '@react-google-maps/api';
import React, {
  createRef,
  RefObject,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import api from '../../utils/api';
import {
  MapSection,
  MapCanvas,
  MapContainer,
  ExitGuideContainer,
  ExitGuide,
  Lote,
  Container,
  Header,
  HeaderContent,
  RastreabilityInfo,
  Producer,
  Culture,
  ProducerInfo,
  BasicInfo,
  ProductInfo,
  ProductImage,
  ProductHistory,
} from './styles';

import logo from '../../assets/feedrastro.svg';

interface Location {
  lat: number;
  lng: number;
}

interface Rastreabilidade {
  id: number;
  lote: string;
  local: {
    nome: string;
    localizacao: Location;
  };
  data: string;
  dias: number;
  qualidade: string;
  certificacao: string;
  unidade: string;
  sacas: number;
  peso: number;
}

interface Documento {
  ge: string;
  data: string;
  safra: string;
  produtor: string;
  avatar: string;
  fazenda: string;
  bebida: {
    tipo: string;
    padrao: string;
    unidade: string;
  };
  locations: Location[];
  rastreabilidades: Rastreabilidade[];
}

interface Resize {
  width: number | undefined;
}

const NewDashboard: React.FC = () => {
  const [data, setData] = useState<Documento[]>([]);
  // const [coordinates, setCoordinates] = useState<Location[]>([]);

  const [dimensions, setDimensions] = React.useState<Resize>({
    width: window.innerWidth / 2,
  });

  const mapCanvasRef = useRef<HTMLDivElement>(null);
  const mapRef = useRef(null);
  const markerRefs = useRef<Array<RefObject<HTMLDivElement> | null>>([]);

  // const [center, setCenter] = useState<Location>();
  const [
    selectedMarker,
    setSelectedMarker,
  ] = useState<Rastreabilidade | null>();

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyD68Cs78a83IY7JjuQM8al3BOa605Ks1sM',
  });

  useEffect(() => {
    api.get('documento').then(response => {
      setData(response.data);
    });
  }, []);

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        width: mapCanvasRef.current?.offsetWidth,
      });
    };

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, [data]);

  useEffect(() => {
    if (data.length > 0) {
      if (markerRefs.current.length !== data[0].rastreabilidades.length) {
        // add or remove refs
        markerRefs.current = Array(data[0].rastreabilidades.length)
          .fill(0)
          .map((_, i) => markerRefs.current[i] || createRef());
      }
    }
  }, [data]);

  const mapContainerStyle = {
    width: dimensions.width,
    height: '100vh',
  };

  const options: google.maps.MapOptions = {
    styles: [
      {
        featureType: 'landscape',
        stylers: [
          { hue: '#FFBB00' },
          { saturation: 43.400000000000006 },
          { lightness: 37.599999999999994 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.highway',
        stylers: [
          { hue: '#FFC200' },
          { saturation: -61.8 },
          { lightness: 45.599999999999994 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.arterial',
        stylers: [
          { hue: '#FF0300' },
          { saturation: -100 },
          { lightness: 51.19999999999999 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'road.local',
        stylers: [
          { hue: '#FF0300' },
          { saturation: -100 },
          { lightness: 52 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'water',
        stylers: [
          { hue: '#0078FF' },
          { saturation: -13.200000000000003 },
          { lightness: 2.4000000000000057 },
          { gamma: 1 },
        ],
      },
      {
        featureType: 'poi',
        stylers: [
          { hue: '#00FF6A' },
          { saturation: -1.0989010989011234 },
          { lightness: 11.200000000000017 },
          { gamma: 1 },
        ],
      },
    ],
    disableDefaultUI: true,
    zoomControl: true,
    scaleControl: false,
    scrollwheel: false,
    gestureHandling: 'greedy',
  };

  const mapOnLoad = useCallback(map => {
    mapRef.current = map;
  }, []);

  if (loadError) {
    // eslint-disable-next-line no-console
    console.log('Erro ao carregar mapa');
    return null;
  }

  return (
    <Container>
      <Header>
        <HeaderContent>
          <img src={logo} alt="Feedrastro" />
          <h1>FEEDRASTRO</h1>
        </HeaderContent>
      </Header>
      <RastreabilityInfo>
        <BasicInfo>
          <Producer>
            <img src={data[0]?.avatar} alt={data[0]?.produtor} />
            <ProducerInfo>
              <div>
                <span>Produtor</span>
                <h2>{data[0]?.produtor}</h2>
              </div>
            </ProducerInfo>
          </Producer>
          <Culture>
            <div>
              <span>Guia de Embarque</span>
              <h3>{data[0]?.ge}</h3>
            </div>
            <div>
              <span>Domicílio</span>
              <h3>{data[0]?.fazenda}</h3>
            </div>
            <div>
              <span>Bebida</span>
              <h3>{data[0]?.bebida.tipo}</h3>
            </div>
            <div>
              <span>Padrão</span>
              <h3>{data[0]?.bebida.padrao}</h3>
            </div>
          </Culture>
        </BasicInfo>
        <ProductInfo>
          <ProductHistory>
            <div className="overlay">
              <p>
                O café é uma bebida produzida a partir dos grãos torrados do
                fruto do cafeeiro. É servido tradicionalmente quente, mas também
                pode ser consumido gelado. O café é um estimulante, por possuir
                cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do
                método de preparação. Estudos têm mostrado que pessoas que bebem
                quatro xícaras de café por dia têm um menor risco de morrer de
                um ataque cardíaco.
              </p>
            </div>
          </ProductHistory>
          <ProductImage />
        </ProductInfo>
      </RastreabilityInfo>
      <MapSection>
        <MapContainer>
          <MapCanvas ref={mapCanvasRef}>
            {isLoaded ? (
              <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={11}
                center={{ lat: -18.734366, lng: -48.193168 }}
                options={options}
                onLoad={mapOnLoad}
              >
                <Polyline
                  path={[
                    {
                      lat: -18.750062,
                      lng: -48.210782,
                    },
                    {
                      lat: -18.669092,
                      lng: -48.177053,
                    },
                  ]}
                  options={{
                    geodesic: true,
                    strokeColor: '#4e7f82',
                    strokeWeight: 5,
                  }}
                />

                <Polyline
                  path={[
                    {
                      lat: -18.761473,
                      lng: -48.18512,
                    },
                    {
                      lat: -18.669092,
                      lng: -48.177053,
                    },
                  ]}
                  options={{
                    geodesic: true,
                    strokeColor: '#27364f',
                    strokeWeight: 5,
                  }}
                />

                {data[0]
                  ? data[0].rastreabilidades.map((marker, i) => (
                      <Marker
                        key={marker.id}
                        position={{
                          lat: marker.local.localizacao.lat,
                          lng: marker.local.localizacao.lng,
                        }}
                        onMouseOver={() => {
                          setSelectedMarker(marker);
                          const currentMarker = markerRefs.current[i];
                          if (currentMarker) {
                            const currentMarkerREf = currentMarker.current;
                            if (currentMarkerREf) {
                              currentMarkerREf.style.borderColor = '#dec20d';
                            }
                          }
                        }}
                        onMouseOut={() => {
                          setSelectedMarker(null);
                          const currentMarker = markerRefs.current[i];
                          if (currentMarker) {
                            const currentMarkerREf = currentMarker.current;
                            if (currentMarkerREf) {
                              currentMarkerREf.style.borderColor = '#99a267';
                            }
                          }
                        }}
                      />
                    ))
                  : null}

                {selectedMarker ? (
                  <InfoWindow
                    options={{
                      disableAutoPan: true,
                      pixelOffset: new google.maps.Size(0, -42),
                    }}
                    position={{
                      lat: selectedMarker.local.localizacao.lat,
                      lng: selectedMarker.local.localizacao.lng,
                    }}
                    onCloseClick={() => {
                      setSelectedMarker(null);
                    }}
                  >
                    <div style={{ textAlign: 'center' }}>
                      <h5>{selectedMarker.local.nome}</h5>
                      <p style={{ marginTop: '10px' }}>{selectedMarker.lote}</p>
                      <p style={{ marginTop: '10px' }}>{selectedMarker.data}</p>
                    </div>
                  </InfoWindow>
                ) : null}
              </GoogleMap>
            ) : null}
          </MapCanvas>
        </MapContainer>
        <ExitGuideContainer>
          <ExitGuide>
            {data[0]
              ? data[0].rastreabilidades.map((marker, i) => (
                  <Lote key={marker.id} ref={markerRefs.current[i]}>
                    <div>
                      <span>Lote:</span>
                      <p>{marker.lote}</p>
                    </div>
                    <div>
                      <span>Data:</span>
                      <p>{marker.data}</p>
                    </div>
                    <div>
                      <span>Certificação:</span>
                      <p>{marker.certificacao}</p>
                    </div>
                  </Lote>
                ))
              : null}
          </ExitGuide>
        </ExitGuideContainer>
      </MapSection>
    </Container>
  );
};

export default NewDashboard;
