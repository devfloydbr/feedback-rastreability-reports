import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  padding: 20px;
  background: #474a2c;

  h1 {
    margin-left: 20px;
  }
`;

export const HeaderContent = styled.div`
  max-width: 1120px;
  margin: 0 auto;

  display: flex;
  align-items: center;
`;

export const RastreabilityInfo = styled.div`
  display: flex;
  flex-direction: column;
  padding: 25px;

  > div + div {
    margin-top: 25px;
  }
`;

export const BasicInfo = styled.div`
  height: 45%;
  display: flex;
  flex-direction: row;
`;

export const Producer = styled.div`
  width: 25%;
  display: flex;
  flex-direction: column;
  align-items: center;

  background: #3b3d24;
  padding: 20px;

  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;

  > img {
    width: 200px;
    height: 200px;

    border-radius: 50%;
  }

  > h2 {
    margin-top: 15px;
  }
`;

export const ProducerInfo = styled.div`
  margin-top: 15px;
  text-align: center;

  div + div {
    margin-top: 15px;
  }
`;

export const Culture = styled.div`
  width: 85%;

  padding: 20px;
  color: #4a4a4a;
  background: #fff;

  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;

  display: flex;
  flex-direction: column;

  justify-content: center;

  div + div {
    margin-top: 15px;
  }
`;

export const ProductInfo = styled.div`
  display: flex;
  flex-direction: row;

  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
`;

export const ProductImage = styled.div`
  width: 25%;
  display: flex;

  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;

  background-image: url('https://images.unsplash.com/photo-1493925410384-84f842e616fb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=401&q=80');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const ProductHistory = styled.div`
  display: flex;
  flex-direction: row-reverse;

  width: 75%;

  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;

  background-image: url('https://images.unsplash.com/photo-1545086421-168708d4f603?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=891&q=80');
  background-repeat: no-repeat;
  background-size: cover;

  .overlay {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;

    display: inline-block;
    height: 100%;
    width: 100%;

    padding: 45px;

    background: rgba(0, 0, 0, 0.4);

    p {
      font-family: 'Caveat', cursive;
      font-size: 2em;

      word-break: break-word;
    }
  }
`;

export const MapSection = styled.div`
  display: flex;
  flex-direction: row;

  height: 100vh;
`;

export const MapContainer = styled.div`
  width: 50%;

  background-color: #fff;
`;

export const MapCanvas = styled.div`
  width: 100%;

  color: #4a4a4a;
`;

export const ExitGuideContainer = styled.div`
  width: 50%;
  height: 100%;
  padding: 25px;

  background: #61794b;

  display: flex;
  flex-direction: column;

  justify-content: center;
`;

export const ExitGuide = styled.div`
  background: #474a2c;

  border-radius: 5px;

  padding: 25px;
  overflow-x: auto;

  div + div {
    margin-top: 10px;
  }
`;

export const Lote = styled.div`
  background: #fff;
  transition: border-radius 0.3s;
  padding: 15px;
  color: #4a4a4a;

  border-radius: 5px;
  border: 3px solid;
  border-color: #99a267;

  transition: border-color linear 0.2s;

  display: flex;
  flex-direction: row;

  justify-content: inherit;
  align-items: baseline;

  div {
    width: 30%;
  }
`;
