import React from 'react';

import { Container } from './styles';

const Header: React.FC = () => (
  <Container>
    <h1>MOCKUP SISTEMA DE RASTREABILIDADE</h1>
  </Container>
);

export default Header;
