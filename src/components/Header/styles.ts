import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100px;

  background-color: #d9c19f;

  box-shadow: 0px 6px 5px -3px rgba(0, 0, 0, 0.59);

  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    color: #fff;
  }
`;
