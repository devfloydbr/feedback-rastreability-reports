import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NewDashboard from '../pages/NewDashboard';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={NewDashboard} />
  </Switch>
);

export default Routes;
